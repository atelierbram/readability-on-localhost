# Custom Stylesheet for Readability Bookmarklet - Favelet

## Self-host your own web stylesheet

- For this *to work* you need a *local server* on your own computer, like [Xampp](http://www.apachefriends.org/en/xampp.html), [Wamp](http://www.wampserver.com/en/), [native Lamp on unix like Linux/Ubuntu/Mac](https://help.ubuntu.com/community/ApacheMySQLPHP) or [IIS on Windows](http://www.iis.net/)
- Just add any bookmarklet favelet to you're bookmarks bar, right click, edit , maybe change it’s name, and paste in the `js/bookmarklet-properties.js` in the 'url'-box, (_edit some path value’s like `yourComputerName`_) and host `readability-js` in your own `localhost/readability/js` folder

#### Bookmarklet Example (_Beatified_)
```javascript
javascript: (function() {
    readConvertLinksToFootnotes = false;
    readStyle = 'style-newspaper';
    readSize = 'size-large';
    readMargin = 'margin-wide';
    _readability_script = document.createElement('script');
    _readability_script.type = 'text/javascript';
    _readability_script.src = 'http://localhost/~yourComputerName/readability/js/readability.js?x=' + (Math.random());
    document.documentElement.appendChild(_readability_script);
    _readability_css = document.createElement('link');
    _readability_css.rel = 'stylesheet';
    _readability_css.href = 'http://localhost/~yourComputerName/readability/css/readability.css';
    _readability_css.type = 'text/css';
    _readability_css.media = 'screen';
    document.documentElement.appendChild(_readability_css);
    _readability_print_css = document.createElement('link');
    _readability_print_css.rel = 'stylesheet';
    _readability_print_css.href = 'http://localhost/~yourComputerName/readability/css/readability-print.css';
    _readability_print_css.media = 'print';
    _readability_print_css.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(_readability_print_css);
})();
```
 
Clone, or download this repository, or make three more folders in `localhost/readability`:
- `js`
- `css`
- `img`

- So `js/readability.js` goes into your own `localhost/readability/js` folder
- Stylesheets go in the css folder: `localhost/readability/css` ; for you’re tweaking pleasures.

---

### Optional
- Grab the small tiling `img/backgr_light.png` and the rest of the images and also put them in your your `img` folder

### Tip
- Typeface: Just use the serif type-face that is set as a default in your browser’s settings, or use a typeface with a large x-height like Lucida Grande, or Georgia.

###  Recommended reading:
- [CSS Design: Going to Print, by Eric Meyer](http://www.alistapart.com/articles/goingtoprint/)
- [ALA’s New Print Styles" by Eric Meyer](http://www.alistapart.com/articles/alaprintstyles/)

by [Atelier Bram de Haan](http://atelierbramdehaan.nl/)
